
var gulp = require('gulp'),

    watchify = require('watchify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    assign = require('lodash.assign'),
    gutil = require('gulp-util'),

    stylus = require('gulp-stylus'),
    nib = require('nib'),

    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    del = require('del'),

    notify = require('gulp-notify');

gulp.task('styles', function() {
  return gulp.src('src/styles/style.styl', { style: 'expanded' })
    .pipe(stylus({ use : nib(), import : ['nib'] }))
    .pipe(gulp.dest('build/css'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('jshint', function() {
  return gulp.src('src/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(notify({ message: 'jshint task complete' }));
});

// Add custom browserify options here
var customOpts = {
  entries: ['./src/scripts/App.js'],
  debug: false
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

gulp.task('browserify', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler

function bundle() {

    return b.bundle()
        .on('error', function (err) {
            console.log(err.toString());
            this.emit("end");
        })
        .pipe(source("main.js"))
        .pipe(buffer())
        .pipe(gulp.dest('./build/js'))
        .pipe(notify({ message: 'Bundle task complete' }));
}

// Tasks
gulp.task('clean', function(cb) {
    del(['build/css', 'build/js'], cb)
});

gulp.task('main', function() {
    gulp.watch('src/styles/**/*.styl', ['styles']);
    gulp.start('jshint', 'browserify');
});

gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts');
});

gulp.task('watch', function() {
  gulp.watch('src/styles/**/*.styl', ['styles']);
  gulp.watch('src/scripts/**/*.js', ['main']);
});


/*
    //.pipe(autoprefixer('last 2 version'))
    //.pipe(rename({suffix: '.min'}))
    //.pipe(minifycss())
    //.pipe(gulp.dest('build/css'))
*/