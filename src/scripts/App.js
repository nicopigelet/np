
var 
	$ = require("jquery"),
	h = require('Handlebars'),

	Event = require('./lib/Event')(),
	Model = require('./lib/Model'),

	helpers = require('./utils/helpers');


var App = {

	URL_TEAMS : "/data/teams.json",
	URL_RIDERS : "/data/individual.json",

	$list : null,

	teamsFetched : false,
	ridersFetched : false,

	$e : $({}),

	init : function() {

		// Listen to events
		this.$e.on( Event.TEAMS_FETCHED, $.proxy( this.handleFetching, this ) );
		this.$e.on( Event.RIDERS_FETCHED, $.proxy( this.handleFetching, this ) );

		// init DOM references
		this.$list = $(".block-list");

		// Create a Loader
		this.initLoader();
	},

	initLoader : function() {

		// Get all JSON (individuals, team, rankings...)
		$.getJSON( this.URL_TEAMS )
			.success( $.proxy(this.fetchedTeams, this) )
			.error(function() {
				console.log("error teams");
			});

		// GET JSON via AJAX
		$.getJSON( this.URL_RIDERS )
			.success( $.proxy(this.fetchedIndividuals, this) )
			.error(function() {
				console.log("error individuals");
			});
	},

	fetchedTeams : function( data ) {

		Model.setTeams( data );
		this.$e.trigger( Event.TEAMS_FETCHED );
	},

	fetchedIndividuals : function( data ) {

		Model.setRiders( data );
		this.$e.trigger( Event.RIDERS_FETCHED );
	},

	handleFetching : function( e ) {

		if ( e.type == Event.TEAMS_FETCHED ) {
			this.teamsFetched = true;
		} else if ( e.type == Event.RIDERS_FETCHED ) {
			this.ridersFetched = true;
		}

		if ( this.teamsFetched && this.ridersFetched ) {

			console.log( "start populating", Model.getTeams(), Model.getRiders() );
			this.populateIndividuals();
		}
	},

	populateIndividuals : function( ) {

		// GET context from data
		var 
			context = { individuals : Model.getRiders() },
			source = $("#template-individual").html(),
			template = h.compile(source),
			html = template(context);

		// Update DOM with html generated
		this.$list.html( html );
	}
};

App.init();
