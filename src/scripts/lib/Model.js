
var $ = require("jquery");

var Model = function Model () {

	var 
		teams = null,
		riders = null;

	this.setTeams = function( data ) {
		teams = data;
	};
	this.getTeams = function() {
		return teams;
	};

	this.setRiders = function( data ) {
		riders = data;
	};
	this.getRiders = function() {
		return riders;
	};

	this.getTeamById = function( team_id ) {

		var selected = null;
		$.each( teams, function( i, el ) {
			if ( el.team_id == team_id ) {
				selected = el;
			}
		} );
		return selected;
	};

	if ( Model.caller != Model.getInstance ) {
		throw new Error( "This object cannot be instanciated" );
	}
};

/* 
 SINGLETON
 */
Model.instance = null;

Model.getInstance = function() {

	if ( this.instance === null ) {
		this.instance = new Model();
	}
	return this.instance;
};

module.exports = Model.getInstance();