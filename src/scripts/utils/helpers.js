
var 
	$ = require('jquery'),
	h = require('Handlebars'),
	Model = require('../lib/Model');

// Helpers
h.registerHelper('lowercase', function ( text ) {
	return text.toLowerCase();
});

h.registerHelper('getBG', function ( id ) {

	var team = Model.getTeamById( id );
	return team.background_color;
});
